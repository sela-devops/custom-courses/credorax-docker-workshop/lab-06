# Docker Workshop
Lab 06: Building with the Docker Build Pattern

---

## Preparations

 - Clone the demo application repository:
```
$ git clone https://github.com/leonjalfon1/java-demo-app.git ~/lab-06
$ cd ~/lab-06
```

 - Ensure that your environment is clean
```
$ docker rm -f $(docker ps -a -q)
$ docker rmi -f $(docker images -q)
```

## Instructions

 - Inspect the Dockerfile
```
$ cat Dockerfile
```

 - Build the docker image

```
$ docker build -t java-demo .
```

 - List the docker images (you will see both base images, the built image and the intermediate image <none>)

```
$ docker images
```

 - Run the created image

```
$ docker run -d --name java-demo -p 8080:8123 java-demo
```

 - Ensure that the images is up and running

```
$ docker ps
```

 - Browse to the application

```
https://<server-ip>:8080/status
```

## Cleanup

 - Clean the environment

```
$ docker rm -f $(docker ps -a -q)
$ docker rmi -f $(docker images -q)
```
